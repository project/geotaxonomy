
GEO-TAXONOMY

Attaches geo information (latitude, longitude, bounding boxes, etc.) to 
taxonomy terms. Provides views integration.

USAGE

1. Install module
2. Create vocabulary, check "Store geo data for this vocabulary"
3. Add term to vocabulary, configure latitude, longitude, etc.

NOTE

Currently not compatible with OpenLayers module. 
http://drupal.org/project/openlayers